import csv
import pandas as pd
import quandl as Quandl
import math
import numpy as np
from sklearn import preprocessing, cross_validation,svm  #preprocessing -> scaling the data for features
from sklearn.linear_model import LinearRegression

df = Quandl.get('WIKI/GOOGL') #Quandl is a finance data website

df = df[['Adj. Open','Adj. High','Adj. Low','Adj. Close','Adj. Volume']]

df['HL_PCT'] = (df['Adj. High'] - df['Adj. Low']) / df['Adj. Low'] * 100.0 #(Hight-low)/low * 100%
df['PCT_change'] =(df['Adj. Close']- df['Adj. Open'])/df['Adj. Open'] *100.0  #(new-old)/old

df = df[['Adj. Close','HL_PCT','PCT_change','Adj. Volume']]
forecast_col = 'Adj. Close'  #what you want to predict
df.fillna(-99999,inplace=True) # put the N/A data to become a outlier 
forecast_out = int(math.ceil(0.01*len(df)))  #math.ceil round to upper whole number  We try to predict 10 percent of data forward.
print(forecast_out) #see how many days we are in advance when predict
df.dropna(inplace=True)
df['label'] = df[forecast_col].shift(-forecast_out) #we shift 32 days to the future (move the current data up 1% days)

#features is X, label is y'
#df.dropna(inplace=True)   ####?
X = np.array(df.drop(['label'],1))  #pandas drop the colum of the list and the axis of it
X = preprocessing.scale(X) #scaling x to normalize it, need to included into the training data as well. 
X_lately = X[-forecast_out:] #Last couple of numbers in X.
X= X[:-forecast_out] #X - X_lately
df.dropna(inplace=True)


y = np.array(df['label'])

X_train, X_test, y_train, y_test = cross_validation.train_test_split(X,y,test_size=0.2)

clf = LinearRegression()#classifier, you can add n_job = 5 or -1(as many as possible)
clf.fit(X_train,y_train)
accuracy = clf.score(X_test,y_test)

clf2 = svm.SVR(kernel='poly')#classifier
clf2.fit(X_train,y_train)
accuracy2 = clf2.score(X_test,y_test)


print(accuracy,accuracy2,forecast_out)



